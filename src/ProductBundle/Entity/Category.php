<?php

namespace ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as SA;

/**
 * Category
 *
 * @ORM\Table("categories")
 * @ORM\Entity
 */
class Category
{
    /**
     * Category ID
     * 
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @SA\Groups({
     *  "get_product_details"
     * })
     * @SA\Type("integer")
     */
    private $id;
    
    /**
     * Products list
     * 
     * @var \Doctrine\Common\Collections\Collection
     * 
     * @ORM\ManyToMany(targetEntity="Product", mappedBy="categories")
     */
    private $products;
    
    /**
     * Parent category
     *
     * @var Category
     * 
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     * 
     * @SA\Groups({
     *  "get_product_details"
     * })
     * @SA\Type("ProductBundle\Entity\Category")
     */
    private $parentCategory;

    /**
     * Category title
     * 
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100)
     * 
     * @SA\Groups({
     *  "get_product_details"
     * })
     * @SA\Type("string")
     */
    private $title;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Category
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->products = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add product
     *
     * @param \ProductBundle\Entity\Product $product
     *
     * @return Category
     */
    public function addProduct(\ProductBundle\Entity\Product $product)
    {
        $this->products[] = $product;

        return $this;
    }

    /**
     * Remove product
     *
     * @param \ProductBundle\Entity\Product $product
     */
    public function removeProduct(\ProductBundle\Entity\Product $product)
    {
        $this->products->removeElement($product);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProducts()
    {
        return $this->products;
    }
    
    /**
     * Get category title
     * 
     * @return string
     */
    public function __toString() {
        return $this->getTitle();
    }

    /**
     * Set parentCategory
     *
     * @param \ProductBundle\Entity\Category $parentCategory
     *
     * @return Category
     */
    public function setParentCategory(\ProductBundle\Entity\Category $parentCategory = null)
    {
        $this->parentCategory = $parentCategory;

        return $this;
    }

    /**
     * Get parentCategory
     *
     * @return \ProductBundle\Entity\Category
     */
    public function getParentCategory()
    {
        return $this->parentCategory;
    }
}
