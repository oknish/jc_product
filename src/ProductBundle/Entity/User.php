<?php

namespace ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as SA;

/**
 * User
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="ProductBundle\Entity\UserRepository")
 * @ORM\HasLifecycleCallbacks
 */
class User {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @SA\Groups({
     *  "get_user_details",
     *  "get_users"
     * })
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * 
     * @SA\Groups({
     *  "get_user_details",
     *  "post_user",
     *  "get_users"
     * })
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="sname", type="string", length=255)
     * 
     * @SA\Groups({
     *  "get_user_details",
     *  "post_user",
     *  "get_users"
     * })
     */
    private $sname;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_update", type="datetime")
     * 
     * @SA\Groups({
     *  "get_user_details",
     *  "get_users"
     * })
     */
    private $lastUpdate;

    /**
     * Phones assigned to user
     * 
     * @var \Doctrine\Common\Collections\ArrayCollection
     * 
     * @ORM\ManyToMany(targetEntity="Phone", inversedBy="users", cascade={"persist"})
     * @ORM\JoinTable(name="user_phones",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="phone_id", referencedColumnName="id")}
     *      )
     * 
     * @SA\Groups({
     *  "get_user_details",
     *  "post_user",
     *  "get_users"
     * })
     * @SA\Accessor(getter="serializePhones", setter="deserializePhones")
     * @SA\Type("array<string>")
     */
    private $phones;

    /**
     * Address assigned to user
     * 
     * @var \Doctrine\Common\Collections\ArrayCollection
     * 
     * @ORM\ManyToMany(targetEntity="Address", inversedBy="users", cascade={"persist"})
     * @ORM\JoinTable(name="user_address",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="address_id", referencedColumnName="id")}
     *      )
     * 
     * @SA\Groups({
     *  "get_user_details",
     *  "post_user",
     *  "get_users"
     * })
     * @SA\Accessor(getter="serializeAddress", setter="deserializeAddress")
     * @SA\Type("array<string>")
     */
    private $address;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return User
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set sname
     *
     * @param string $sname
     *
     * @return User
     */
    public function setSname($sname) {
        $this->sname = $sname;

        return $this;
    }

    /**
     * Get sname
     *
     * @return string
     */
    public function getSname() {
        return $this->sname;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return User
     */
    public function setLastUpdate($lastUpdate = null) {
        $this->lastUpdate = new \DateTime();

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate() {
        return $this->lastUpdate;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->phones = new \Doctrine\Common\Collections\ArrayCollection();

        $this->setLastUpdate(new \DateTime);
    }

    /**
     * Add phone
     *
     * @param \ProductBundle\Entity\Phone $phone
     *
     * @return User
     */
    public function addPhone(\ProductBundle\Entity\Phone $phone) {
        $this->phones[] = $phone;

        return $this;
    }

    /**
     * Remove phone
     *
     * @param \ProductBundle\Entity\Phone $phone
     */
    public function removePhone(\ProductBundle\Entity\Phone $phone) {
        $this->phones->removeElement($phone);
    }

    /**
     * Get phones
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPhones() {
        return $this->phones;
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist() {
        $this->setLastUpdate();
    }

    /**
     * 
     * @return array<string>
     */
    public function serializePhones() {
        $result = [];

        if (!empty($this->phones)) {
            foreach ($this->phones as $phone) {
                /* @var $phone Phone */
                $result[] = $phone->getPhone();
            }
        }

        return $result;
    }

    /**
     * 
     * @return array<string>
     */
    public function serializeAddress() {
        $result = [];

        if (!empty($this->address)) {
            foreach ($this->address as $address) {
                /* @var $phone Phone */
                $result[] = $address->getAddress();
            }
        }

        return $result;
    }

    /**
     * 
     */
    public function deserializeAddress(array $address) {
        if (!empty($address)) {
            foreach ($address as $addressRow) {
                $addressObj = new Address();
                $addressObj->setAddress($addressRow);
                
                $this->addAddress($addressObj);
            }
        }

        return $this;
    }
    /**
     * 
     */
    public function deserializePhones(array $phones) {
        if (!empty($phones)) {
            foreach ($phones as $phone) {
                $phoneObj = new Phone();
                $phoneObj->setPhone($phone);
                
                $this->addPhone($phoneObj);
            }
        }

        return $this;
    }
    /**
     * Add address
     *
     * @param \ProductBundle\Entity\Address $address
     *
     * @return User
     */
    public function addAddress(\ProductBundle\Entity\Address $address)
    {
        $this->address[] = $address;

        return $this;
    }

    /**
     * Remove address
     *
     * @param \ProductBundle\Entity\Address $address
     */
    public function removeAddress(\ProductBundle\Entity\Address $address)
    {
        $this->address->removeElement($address);
    }

    /**
     * Get address
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAddress()
    {
        return $this->address;
    }
}
