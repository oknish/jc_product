<?php

namespace ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as SA;

/**
 * Phone
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="ProductBundle\Entity\PhoneRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Phone
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;    
    
    /**
     * 
     * Users list
     * 
     * @var \Doctrine\Common\Collections\Collection
     * 
     * @ORM\ManyToMany(targetEntity="User", mappedBy="phones")
     */
    private $users;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255)
     * 
     * @SA\Groups({
     *  "get_user_details",
     *  "post_user",
     *  "get_users"
     * })
     */
    private $phone;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_update", type="datetime")
     */
    private $lastUpdate;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return Phone
     */
    public function setLastUpdate($lastUpdate = null)
    {
        $this->lastUpdate = new \DateTime();

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
        $this->setLastUpdate(new \DateTime);
    }

    /**
     * Add user
     *
     * @param \ProductBundle\Entity\User $user
     *
     * @return Phone
     */
    public function addUser(\ProductBundle\Entity\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \ProductBundle\Entity\User $user
     */
    public function removeUser(\ProductBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }
    
    /**
     * @ORM\PrePersist
     */
    public function onPrePersist() {
        $this->setLastUpdate();
    }
}
