<?php

namespace ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as SA;

/**
 * Image
 *
 * @ORM\Table("images")
 * @ORM\Entity
 */
class Image
{
    /**
     * Image ID
     * 
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Image URL
     * 
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     * 
     * @SA\Groups({
     *  "get_product_details"
     * })
     * @SA\Type("string")
     */
    private $url;

    /**
     * Image description
     * 
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=100)
     * 
     * @SA\Groups({
     *  "get_product_details"
     * })
     * @SA\Type("string")
     */
    private $description;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Image
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Image
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * Get image title
     * 
     * @return string
     */
    public function __toString() {
        return $this->getDescription();
    }
}
