<?php

namespace ProductBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\View\View;
use JMS\Serializer\SerializationContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use ProductBundle\Entity\User;
use FOS\RestBundle\Controller\FOSRestController;
use ProductBundle\Entity\Product;

/**
 * Product controller.
 *
 */
class ApiProductController extends FOSRestController {

    /**
     * Get full user information by ID
     * @ApiDoc(
     *      section = "Users",
     *      requirements={
     *          {
     *              "name"="userId",
     *              "dataType"="integer",
     *              "description"="User ID"
     *          }
     *      },
     *      output = {
     *          "class" = "ProductBundle\Entity\User",
     *          "groups" = {"get_user_details"}
     *      }
     * )
     *
     * @Rest\View()
     * @Route("/users/{userId}")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getUserDetailsAction($userId) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ProductBundle:User')->find($userId);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $sc = SerializationContext::create();
        $sc->setGroups(array($this->get('request')->get('_route')));

        $view = new View($entity);
        $view->setSerializationContext($sc);

        return $this->handleView($view); //->getResponse();
    }

    /**
     * Add new user
     * @ApiDoc(
     *      section = "Users",
     *      input={
     *          "class" = "ProductBundle\Entity\User",
     *          "groups" = {"post_user"}
     *      },
     *      output = {
     *          "class" = "ProductBundle\Entity\User",
     *          "groups" = {"get_user_details"}
     *      }
     * )
     *
     * @Rest\View()
     * @Route("/users")
     * @ParamConverter("user", converter="fos_rest.request_body")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postUserAction(User $user) {
        $em = $this->getDoctrine()->getManager();

        $em->persist($user);
        $em->flush();
        $sc = SerializationContext::create();
        $sc->setGroups(array("get_user_details"));

        $view = new View($user);
        $view->setSerializationContext($sc);

        return $this->handleView($view); //->getResponse();
    }

    /**
     * Get full users list
     * @ApiDoc(
     *      section = "Users",
     *      requirements={
     *      },
     *      output = {
     *          "class" = "ProductBundle\Entity\User",
     *          "groups" = {"get_users"}
     *      }
     * )
     *
     * @Rest\View()
     * @Route("/users")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getUsersAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ProductBundle:User')->findAll();

        $sc = SerializationContext::create();
        $sc->setGroups(array($this->get('request')->get('_route')));

        $view = new View($entities);
        $view->setSerializationContext($sc);

        return $this->handleView($view); //->getResponse();
    }

    /**
     * Remove user by ID
     * @ApiDoc(
     *      section = "Users",
     *      requirements={
     *          {
     *              "name"="userId",
     *              "dataType"="integer",
     *              "description"="User ID"
     *          }
     *      }
     * )
     *
     * @Rest\View()
     * @Route("/users/{userId}")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteUserAction($userId) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ProductBundle:User')->find($userId);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $em->remove($entity);
        $em->flush();

        $sc = SerializationContext::create();
        $sc->setGroups(array($this->get('request')->get('_route')));

        $view = new View($entity);
        $view->setSerializationContext($sc);

        return $this->handleView($view); //->getResponse();
    }

    /**
     * Change user
     * @ApiDoc(
     *      section = "Users",
     *      input={
     *          "class" = "ProductBundle\Entity\User",
     *          "groups" = {"get_user_details"}
     *      },
     *      output = {
     *          "class" = "ProductBundle\Entity\User",
     *          "groups" = {"get_user_details"}
     *      }
     * )
     *
     * @Rest\View()
     * @Route("/users")
     * @ParamConverter("user", converter="fos_rest.request_body")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function putUserAction(User $user) {
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository('ProductBundle:User')->find($user->getId());

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }
        
        $em->merge($user);
        $em->flush();
        $sc = SerializationContext::create();
        $sc->setGroups(array("get_user_details"));

        $view = new View($user);
        $view->setSerializationContext($sc);

        return $this->handleView($view); //->getResponse();
    }

}
