Product test project.

Installation:

1. Get composser - https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx
2. Clone this repository to dome folder:

```
#!sh
git clone https://bitbucket.org/oknish/jc_product.git /var/sites/jc_product
```

3. Configure web server, example for apache2:

```
#!cfg

<VirtualHost *:80>
        ServerAdmin admin@ac-team.ru

        ServerName xutpuk.pp.ua

        DocumentRoot /var/sites/jc_product/web/
        <Directory />
                Options FollowSymLinks
                AllowOverride All
        </Directory>
        <Directory /var/sites/jc_product/web/>
                Options Indexes FollowSymLinks MultiViews
                AllowOverride All
                Order allow,deny
                allow from all
        </Directory>

        ErrorLog ${APACHE_LOG_DIR}/error_jc_product.log

        # Possible values include: debug, info, notice, warn, error, crit,
        # alert, emerg.
        LogLevel warn

        CustomLog ${APACHE_LOG_DIR}/access_jc_product.log combined
</VirtualHost>
```
4. Check apache configs before restart, and reload configs:

```
#!sh

sudo apachectl configtest
Syntax OK

sudo service apache2 reload
 * Reloading web server config apache2 

```
5. Change directory to project home, and install project dependencies:

```
#!sh

cd /var/sites/jc_product/
composer install
```
Follow installation hints and configure DB:

```
#!sh

Creating the "app/config/parameters.yml" file
Some parameters are missing. Please provide them.
database_host (127.0.0.1):
database_port (null):
database_name (symfony): product
database_user (root):
database_password (null): SomePassword
mailer_transport (smtp):
mailer_host (127.0.0.1):
mailer_user (null):
mailer_password (null):
```
6. Run symfony commands to create DB and schema:

```
#!sh

app/console doctrine:database:create
Created database `product` for connection named default

app/console doctrine:schema:create
```

7. Open homepage in browser.